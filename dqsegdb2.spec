%define name    dqsegdb2
%define version 1.0.1
%define release 3

Name:      %{name}
Version:   %{version}
Release:   %{release}%{?dist}
Summary:   Simplified python interface to DQSEGDB

License:   GPLv3
Url:       https://pypi.org/project/%{name}/
Source0:   https://pypi.io/packages/source/d/%{name}/%{name}-%{version}.tar.gz

Vendor:    Duncan Macleod <duncan.macleod@ligo.org>

BuildArch: noarch

BuildRequires: rpm-build
BuildRequires: python-rpm-macros
BuildRequires: python-srpm-macros
BuildRequires: python3-rpm-macros

# python3x-dqsegdb2
BuildRequires: python%{python3_pkgversion}-setuptools
BuildRequires: python%{python3_pkgversion}-ligo-segments
BuildRequires: python%{python3_pkgversion}-gwdatafind
BuildRequires: python%{python3_pkgversion}-pytest

%description
DQSEGDB2 is a simplified Python implementation of the DQSEGDB API as defined in
LIGO-T1300625.
This package only provides a query interface for `GET` requests, any users
wishing to make `POST` requests should refer to the official `dqsegdb` Python
client available from https://github.com/ligovirgo/dqsegdb/.

# -- python3x-gwdatafind

%package -n python%{python3_pkgversion}-%{name}
Summary:  Simplified Python %{python3_version} interface to DQSEGDB
Requires: python%{python3_pkgversion}-ligo-segments
Requires: python%{python3_pkgversion}-gwdatafind
%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}
%description -n python%{python3_pkgversion}-%{name}
DQSEGDB2 is a simplified Python implementation of the DQSEGDB API as defined in
LIGO-T1300625.
This package only provides a query interface for `GET` requests, any users
wishing to make `POST` requests should refer to the official `dqsegdb` Python
client available from https://github.com/ligovirgo/dqsegdb/.

# -- build steps

%prep
%autosetup -n %{name}-%{version}

%build
%py3_build

%check
%{__python3} -m pytest --pyargs %{name}

%install
%py3_install

%clean
rm -rf $RPM_BUILD_ROOT

%files -n python%{python3_pkgversion}-%{name}
%license LICENSE
%doc README.md
%{python3_sitelib}/*

# -- changelog

%changelog
* Thu Apr 28 2022 Alexander Pace <alexander.pace@ligo.org> - 1.0.1-3
- stripped out python2 for el8 gstlal packaging.

* Thu Feb 07 2019 Duncan Macleod <duncan.macleod@ligo.org> - 1.0.1-1
- first release
